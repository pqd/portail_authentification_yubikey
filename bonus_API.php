<?php 
/* 
 * API pour vérifier la validation d'une clé yubikey sur un serveur yk-val de son choix
 *
 * Auteur : PQD
 * Version : 1.0
 * Date : 07 mars 2021
 * 
 *
 */

include 'variables.php';

function nonce($longueur)
{
// Cette fonction génère une chaîne de caractères pseudo-aléatoire d'une longueur
// définie par l'utilisateur lorsqu'il l'appelle.
// Entrée : longueur souhaitée
// Sortie : chaîne de caractère
	  $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	  $chaine = '';
	  for ($i = 0; $i < $longueur; $i++)
	  {
		  $chaine .= $chars[rand(0, $longueur - 1)];
			      }
	    return $chaine;
}
echo "<HTML>";
echo "<HEAD><TITLE>Portail d'authentification</TITLE></HEAD>";
echo "<BODY>";
echo "Validation OTP";
echo " <br>";
echo "<FORM action=\"\" method=\"POST\">";
echo "<input name=\"HMAC\" type=\"texte\" placeholder=\"Appuyez sur votre yubikey\" size=\"44\" maxlength=\"44\">";
echo "</FORM>";
// Traitement si une clé HMAC OTP a été saisie
if (isset($_POST['HMAC']))
{
	$otp = ($_POST['HMAC']);
	$serial = (substr($otp,0,12));

	//Vérification de la Yubikey, si c'est une des nôtres, on traite la saisie
	if ($serial == $numserie1) // si plusieurs Yubikeys, mettez des opérateurs ||
	{
		$nonce = nonce(16);
		$url = $adresse;
		$options = array("id"=>1,"nonce"=>$nonce,"otp"=>$otp);
		$url .= http_build_query($options,'','&');
		$reponse = file_get_contents($url) or die(print_r(error_get_last()));
		// Si la clé est reconnue et l'OTP valide
		if(stristr($reponse, 'status=OK') == TRUE)
		{
			echo "OTP valide (".$otp.").";
		}
		else
		{
			// La clé est reconnue, l'OTP n'est plus valide (rejoué ou incorrect)
			echo "OTP invalide.";

		}
	}
	else 
	{
		// La clé n'est pas reconnue
		echo "OTP invalide.";
	}
}
else
{
	// En l'absence de saisie
	echo "OTP attendu.";
}
echo "</BODY>";
echo "</HTML>";
?>

