<?php 
// Mettre ce code avant toute page du site (conservation de la session)
session_start();
if (isset($_SESSION['login']))
{
	// On se trouve tjs dans la session, la page sera chargée
}
else
{
	// Sinon en renvoie au portail d'authentificaiton
        header('Location: portail.html');
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<HEAD>
<meta http-equiv="pragma" content="no-cache">
<META http-equiv="Cache-Control" content="no-cache, proxy-revalidate">
</HEAD>
<BODY>
Page quelconque une fois l'authentification r&eacute;ussie<br>
<br>
OTP utilis&eacute; : <?php echo $_SESSION['login']; ?>
<br><br><br>
<a href="logout.php">Cliquez ici pour vous d&eacute;connecter</a>
</BODY>
</HTML>
