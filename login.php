<?php 
/* Script d'authentification par Yubikey (HMAC OTP)
 *
 * Auteur : PQD
 * Version : 1.0
 * Date : 06 mars 2021
 * 
 * Validation d'un HOTP Yubikey via un serveur yk-val de votre choix et
 * ouvrir d'une session PHP en cas de succès.
 * 
 *
 */

include 'variables.php';

function nonce($longueur)
{
// Cette fonction génère une chaîne de caractères pseudo-aléatoire d'une longueur
// définie par l'utilisateur lorsqu'il l'appelle.
// Entrée : longueur souhaitée
// Sortie : chaîne de caractère
	  $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	  $chaine = '';
	  for ($i = 0; $i < $longueur; $i++)
	  {
		  $chaine .= $chars[rand(0, $longueur - 1)];
			      }
	    return $chaine;
}

// Traitement si une clé HMAC OTP a été saisie
if (isset($_POST['HMAC']))
{
	$otp = ($_POST['HMAC']);
	$serial = (substr($otp,0,12));

	//Vérification de la Yubikey, si c'est une des nôtres, on traite la saisie
	if ($serial == $numserie1) // si plusieurs Yubikeys, mettez des opérateurs ||
	{
		$nonce = nonce(16);
		$url = $adresse;
		$options = array("id"=>$id,"nonce"=>$nonce,"otp"=>$otp);
		$url .= http_build_query($options,'','&');
		$reponse = file_get_contents($url) or die(print_r(error_get_last()));
		// Si la clé est reconnue et l'OTP valide, on ouvre une session.
		if(stristr($reponse, 'status=OK') == TRUE)
		{
			session_start();
			$_SESSION['login'] = $_POST['HMAC'];
			header ('Location:index.php');

		}
		else
		{
			// La clé est reconnue, l'OTP n'est plus valide (rejoué ou incorrect)
			echo '<body onLoad="alert(\'OTP invalide\')">';
			echo '<meta http-equiv="refresh" content="0;URL=portail.html">';

		}
	}
	else 
	{
		// La clé n'est pas reconnue
		echo '<body onLoad="alert(\'OTP invalide\')">';
		echo '<meta http-equiv="refresh" content="0;URL=portail.html">';
	}
}
else
{
	// En l'absence de saisie par sécurité
	echo '<meta http-equiv="refresh" content="0;URL=portail.html">';
}
?>

