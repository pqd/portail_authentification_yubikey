## Portail d'authentification par Yubikey HOTP en PHP ##

Ce projet a pour but de permettre l'authentification sur un site web par Yubikey HOTP sans utiliser les serveurs de Yubico.

### Prérequis ###

- PHP >= 7 de préférence (je n'ai pas testé sur version inférieure), Apache2;
- Linux Debian 9 ou 10, la compatibilité n'est pas vérifiée sur les autres plateformes;
- allow_url_fopen = On dans /etc/php/X.X/apache2/php.ini (ou 1 si PHP < 7) ;
- Communiquer avec un serveur de validation yk-val;
- ... Une ou plusieurs yubikey :)

### Fonctionnement ###

- Les numéros de séries de clés autorisées, l'URL du serveur yk-val et l'id d'API se toruvent dans variables.php;
- Une vérification de session est faite sur chaque page visitée du site;
- La première visite étant hors session, les visiteurs sont renvoyés vers portail.html;
- La page portail.html demande l'OTP de la Yubikey et le transmet à login.php;
- La page login.php vérifie l'OTP et valide l'accès;
- La page logout.php permet de terminer puis détruire la session en cours.

### Fonctionnement de login.php ###

- Vérification du numéro de série de la clé présentée;
- Si le numéro de série est connu, validation par serveur yk-val de son choix; 
- En cas de succès, les visiteurs sont renvoyés vers la page index.php qui vérifie la présence d'une session valide - comme chaque page du site - (nom de session = token);
- En cas d'échec les visiteurs sont invités à rentrer un nouveau code OTP. 

### Remarques ####

Comme indiqué en commentaire de index.php, chaque page du site devra contenir le code de validation de session.
Il est possible de coupler l'OTP avec un login et mot de passe à rajouter dans les variables de session.
Il est également possible de renseigner plusieurs clés. 
